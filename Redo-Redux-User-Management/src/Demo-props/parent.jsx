import React, { Component } from "react";
import Children from "./children";
export default class Parent extends Component {
  movie = {
    name: "Home",
    rating: 5,
  };
  render() {
    return (
      <div>
        <h1>Parent Component</h1>
        <Children item={this.movie} />
      </div>
    );
  }
}
