import React, { Component } from "react";

export default class Children extends Component {
  render() {
    console.log("pros", this.props.item);
    // use tech ES6 detruscting: bóc tách phần tử
    const { name, rating } = this.props.item;
    return (
      <div>
        <h1>Child Component</h1>
        <h3>movie name:{this.props.item.name}</h3>
        <h3>movie name:{this.props.item.rating}</h3>
        {/*use destructing*/}
        <h3>movie name:{name}</h3>
        <h3>movie name:{rating}</h3>
        {/*<GrandChil itemGrand={this.props.item} />*/}
      </div>
    );
  }
}
