import React from "react";
import Carousel from "../carousel";
import Content from "../content";
import Header from "../header";
import Footer from "../footer";
export default class Home extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel />
        <Content />
        <Footer />
      </div>
    );
  }
}
