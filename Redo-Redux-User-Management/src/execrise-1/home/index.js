import React from "react";
import Header from "../header";
import Content from "../content";
import Slidebar from "../slidebar";
import Footer from "../footer";
import "./style.css";
class Home extends React.Component {
  render() {
    return (
      <div>
        <Header></Header>
        <div className="container">
          <Content></Content>
          <Slidebar></Slidebar>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}
export default Home;
