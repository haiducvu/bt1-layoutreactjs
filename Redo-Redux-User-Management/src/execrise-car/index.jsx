import React, { Component } from "react";
import RedCar from "../assets/img/red-car.jpg";
import BlackCar from "../assets/img/black-car.jpg";
import Silver from "../assets/img/silver-car.jpg";
export default class Car extends Component {
  state = {
    car: RedCar,
    a: 1,
  };

  //car = RedCar;
  //function handles // có tham số th2
  changeColor = (img) => () => {
    // this.car = img;
    // console.log(this.car);
    this.setState(
      {
        car: img,
      },
      () => {
        console.log(this.state.car);
      }
    );
    console.log(this.state.car);
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <img className="w-100" src={this.state.car} alt="Car" />
          </div>
          <div className="col-6">
            <p>Change Color</p>
            <button
              className="btn btn-danger mr-3"
              onClick={this.changeColor(RedCar)}
            >
              Red
            </button>
            <button
              className="btn btn-secondary mr-3"
              onClick={this.changeColor(Silver)}
            >
              Silver
            </button>
            <button
              className="btn btn-dark mr-3"
              onClick={this.changeColor(BlackCar)}
            >
              Black
            </button>
          </div>
        </div>
      </div>
    );
  }
}
