import React, { Component } from "react";
import ProductIem from "./../ProductItem";

export default class ProductList extends Component {
  //this.props.data  //functuion props
  // renderProducts = () => {
  //   return this.props.data.map((item, index) => {
  //     return (
  //       <div className="col-4" key={index}>
  //         <ProductIem />
  //       </div>
  //     );
  //   });
  // };

  render() {
    return (
      <div className="container">
        <h1 className="display-4">Danh sách sản phẩm</h1>
        {/**<div className="row">{this.renderProducts()}</div> */}
        <div className="row">
          {/**
            <div className="col-md-4">
            <ProductIem />
          </div>
          <div className="col-md-4">
            <ProductIem />
          </div>
          <div className="col-md-4">
            <ProductIem />
          </div>
          */}
          {this.props.data.map((item, index) => {
            return (
              <div className="col-4" key={index}>
                <ProductIem
                  prod={item}
                  getProduct={this.props.getProduct}
                  putToCart={this.props.putToCart}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
