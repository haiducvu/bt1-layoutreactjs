import React, { Component } from "react";

export default class CartItem extends Component {
  handleDeleteCartItem = () => {
    this.props.deleteItem(this.props.item.product.id);
  };
  render() {
    const { img, name, price } = this.props.item.product;
    const { quantity } = this.props.item;
    return (
      <tr>
        <td>
          <img style={{ width: 200 }} src={img} />
        </td>
        <td style={{ fontSize: 25 }}>{name}</td>
        <td>150$</td>
        <td>
          1
          <div className="btn-group">
            <button className="btn btn-info border-right">-</button>
            <button className="btn btn-info border-left">+</button>
          </div>
        </td>
        <td>{price * quantity}</td>
        <td>
          <button className="btn btn-info" onClick={this.handleDeleteCartItem}>
            x
          </button>
        </td>
      </tr>
    );
  }
}
