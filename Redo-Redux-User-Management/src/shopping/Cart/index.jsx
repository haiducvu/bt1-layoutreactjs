import React, { Component } from "react";
import CartItem from "./../Cart_Item";
class Cart extends Component {
  renderCartItem = () => {
    //kiểm tra nếu giỏ hàng rỗng => <h4>ko có sản phẩm</h4>
    //else =>[cartitem,cartitem,cartitem]
    if (this.props.cart.length) {
      return this.props.cart.map((item, index) => {
        return (
          <CartItem
            item={item}
            key={index}
            deleteItem={this.props.deleteItem}
          />
        );
      });
    }
    return (
      <tr>
        <td colSpan="100%" className="text-center font-weight-bold font-italic">
          Không có sản phẩm
        </td>
      </tr>
    );
  };

  calcTotalAmount = () => {
    return this.props.cart.reduce((total, item) => {
      return item.product.price * item.quantity + total;
    }, 0); // 0 là giá trị ban đầu của total
  };
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div
          className="modal-dialog"
          role="document"
          style={{ maxWidth: 1000 }}
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Giỏ hàng</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>

            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>Hình Ảnh</th>
                    <th>Sản Phẩm</th>
                    <th>Giá</th>
                    <th>Số Lượng</th>
                    <th>Tổng Cộng</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {/**  <CartItem />
                   <CartItem />
                   <CartItem />*/}
                  {this.renderCartItem()}
                  {this.props.cart.length > 0 && (
                    <tr>
                      <td />
                      <td />
                      <td />
                      <td style={{ fontSize: 30 }} className="font-weight-bold">
                        Tổng Tiền
                      </td>
                      <td style={{ fontSize: 30 }} className="font-weight-bold">
                        {this.calcTotalAmount()}
                      </td>
                      <td>
                        <button
                          style={{ fontSize: 30 }}
                          className="btn btn-info"
                        >
                          Thanh Toán
                        </button>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>

            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Cart;
