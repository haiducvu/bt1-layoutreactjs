import React, { Component } from "react";
import classes from "./style.module.css";

// chỉ có state đổi thì giao diện mới đổi => biến isShowDes thành state
export default class ProductItem extends Component {
  state = {
    isShowDes: false,
  };
  toggleDescription = () => {
    this.setState({
      isShowDes: !this.state.isShowDes,
    });
  };
  handleViewDetail = () => {
    this.props.getProduct(this.props.prod);
    console.log("Press");
  };

  handlePutToCart = () => {
    this.props.putToCart(this.props.prod);
  };
  render() {
    const { img, name, desc } = this.props.prod;
    return (
      <div className="card ">
        <img src={img} alt="product" className={classes.productImg}></img>
        <div className="card-body">
          <h4>{name}</h4>
          {/*this.isShowDes ? <p>{desc}</p> : null*/}
          {/*this.isShowDes && <p>{desc}</p>*/}
          {this.state.isShowDes && <p>{desc}</p>}
          <button className="btn btn-success" onClick={this.handleViewDetail}>
            Xem chi tiết
          </button>
          <button
            className="btn btn-info ml-2"
            onClick={this.toggleDescription}
          >
            {this.state.isShowDes ? " Ẩn mô tả" : "Hiện mô tả"}
          </button>
          <button className="btn btn-primary" onClick={this.handlePutToCart}>
            Giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
