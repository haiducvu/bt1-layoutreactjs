//demo databinding
//Cách giao tiếp UI và JS
import React, { Component } from "react";

export default class Databinding extends Component {
  //phương thức
  name = "Duc Hai";
  isHandsome = true;
  checkHandsome() {
    if (this.isHandsome) return "Yes";
    return "No";
  }

  //th1:func bình thường, ko tham số
  showMessage() {
    console.log("abc");
  }
  //th2:func có tham số đầu vào
  //C1: ES5
  showMessagewithParams(mesage) {
    return function () {
      console.log(mesage);
    };
    // gọi là: closure
  }
  //C2: ES6
  showMessagewithParamsArrowFuc = (message) => () => {
    console.log(message);
  };
  //th3: hàm có con trỏ this //ArowFuc ko làm tham đổi ngữ cảnh của This
  //   showMessageWithThis() {
  //     console.log(this.name);
  //   } //this lỗi, chuyển về AF
  showMessageWithThis = () => {
    console.log(this.name);
  };
  //thuộc tính
  render() {
    let age = 12;
    return (
      <div>
        <h1>Demo Databinding</h1>
        <h3>Author:{this.name}</h3>
        <h3>Age:{age}</h3>
        {/*C1: Tạo Hàm <h3>Handsome:{this.checkHandsome()}</h3>*/}
        <h3>Handsome:{this.isHandsome ? "Yes" : "No"}</h3>

        <button onClick={this.showMessage}>Show Message</button>
        <button onClick={this.showMessagewithParams("hai")}>
          Show MessageWithParams
        </button>
        <button onClick={this.showMessageWithThis}>
          Show Message With This
        </button>
      </div>
    );
  }
}
