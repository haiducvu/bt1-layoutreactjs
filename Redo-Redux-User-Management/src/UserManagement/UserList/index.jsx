import React, { Component } from "react";
import UserItem from "../UserItem";
import { connect } from "react-redux";
class Users extends Component {
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            {/**
            <UserItem />
            <UserItem />
            <UserItem />
            */}
            {this.props.users.map((item, index) => {
              return <UserItem item={item} key={index} />;
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
//lấy data từ store để=> render
//state là đại diện data lấy trên store về, khi sử dụng props về
const mapStatetoProps = (state) => {
  //return về object
  return {
    users: state.userList,
  };
};
export default connect(mapStatetoProps)(Users);
