import React, { Component } from "react";
import { connect } from "react-redux";
class Modal extends Component {
  handleCancel = () => {
    const action = {
      type: "CANCEL",
    };
    this.props.dispatch(action);
  };

  //handle ADD NEW USERS, get data on FORM input keyboard
  //ta mong muốn có 1 user có structure như vầy
  state = {
    user: {
      username: "",
      name: "",
      email: "",
      phoneNumber: "",
      type: "",
    },
  };
  //

  handleChange = (event) => {
    console.log(event.target.name);
    this.setState({
      user: { ...this.state.user, [event.target.name]: event.target.value },
    });
  };

  handleSubmit = (event) => {
    event.preventDefault(); //block load page;
    console.log(this.state.user);
    if (this.props.selectedUser) {
      this.props.dispatch({
        type: "UPDATE_USER",
        payload: this.state.user,
      });
    } else {
      //gởi yc lên store để add user
      this.props.dispatch({
        type: "ADD_USER",
        payload: this.state.user,
      });
    }
  };

  render() {
    return (
      <div>
        <div
          style={{
            background: "rgba(0,0,0,0.7)",
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
          }}
        >
          <div className="bg-white w-50 mx-auto px-5 pb-3 rounded ">
            <h1 className="text-center display-4 m-0">Form User</h1>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label>Username</label>
                <input
                  type="text"
                  name="username"
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.user.username}
                />
              </div>
              <div className="form-group">
                <label>Name</label>
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.user.name}
                />
              </div>
              <div className="form-group">
                <label>Email</label>
                <input
                  type="text"
                  name="email"
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.user.email}
                />
              </div>
              <div className="form-group">
                <label>Phone Number</label>
                <input
                  type="text"
                  name="phoneNumber"
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.user.phoneNumber}
                />
              </div>
              <div className="form-group">
                <label>Type</label>
                <select
                  className="form-control"
                  onChange={this.handleChange}
                  name="type"
                  value={this.state.user.type}
                >
                  <option>USER</option> <option>VIP</option>
                </select>
              </div>
              <button type="submit" className="btn btn-success">
                Submit
              </button>
              <button
                type="button"
                className="btn btn-warning ml-2"
                onClick={this.handleCancel}
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
  //tự động chạy sau render
  componentDidMount() {
    this.props.selectedUser &&
      this.setState({
        user: this.props.selectedUser,
      });
  }
  componentWillUnmount() {
    this.props.dispatch({
      type: "SELECT_USER",
      payload: null,
    });
  }
}

//edit button
const mapStatetoProps = (state) => {
  return {
    selectedUser: state.selectedUser,
  };
};
export default connect(mapStatetoProps)(Modal);
