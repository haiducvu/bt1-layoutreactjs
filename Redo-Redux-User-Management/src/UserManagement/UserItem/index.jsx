import React, { Component } from "react";
import { connect } from "react-redux";
class UserItem extends Component {
  handleEditUser = () => {
    const action = {
      type: "SHOW_DATA",
    };
    this.props.dispatch(action);
    //dispatch  action set selectUser
    this.props.dispatch({
      type: "SELECT_USER",
      payload: this.props.item,
    });
  };

  render() {
    //destructuring
    const { name, username, email, phoneNumber, type } = this.props.item;
    return (
      <tr>
        <td>{name}</td>
        <td>{username}</td>
        <td>{email}</td>
        <td>{phoneNumber}</td>
        <td>{type}</td>
        <td>
          <button className="btn btn-info mr-2" onClick={this.handleEditUser}>
            Edit
          </button>
          <button className="btn btn-danger">Delete</button>
        </td>
      </tr>
    );
  }
}

export default connect()(UserItem);
