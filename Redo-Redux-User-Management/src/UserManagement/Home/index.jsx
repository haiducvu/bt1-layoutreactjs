import React, { Component } from "react";
import Search from "../Search";
import Users from "../UserList";
import Modal from "../Modal";
import { connect } from "react-redux";
class Home extends Component {
  //hàm để chỉnh sửa data từ store, nút add user
  handleUser = () => {
    //trước khi dispatch<= connect redux-reducer trước. dispatch send action lên store
    const action = {
      // type <= 'yêu cầu làm gì khi gởi'
      type: "SHOW_DATA",
    };
    //sau đó truyền action vào dispatch
    this.props.dispatch(action);
  };
  render() {
    return (
      <div className="container">
        <h1 className="display-4 text-center my-3">User Management</h1>
        <div className="d-flex justify-content-between align-items-center">
          <Search />
          <button className="btn btn-success" onClick={this.handleUser}>
            Add User
          </button>
        </div>
        <Users />
        {this.props.ishow && <Modal />}
      </div>
    );
  }
}
//hàm để lấy data từ trên store về để render
//state là đại diện tất cả data đang có trên store
//return về object => để sài chuyển thành props, lấy về đặt lại tên
const mapSatetoProps = (state) => {
  return {
    //object
    ishow: state.showModal,
  };
};
export default connect(mapSatetoProps)(Home);
