//giá trị mặc định, khởi tạo của userList
let initialState = [
  {
    id: 1,
    name: "Vũ Đức Hải 1",
    username: "vuduchai1",
    email: "vudchai1@gmail.com",
    phoneNumber: "123456",
    type: "VIP",
  },
  {
    id: 2,
    name: "Vũ Đức Hải 2",
    username: "vuduchai2",
    email: "vuduchai2@gmail.com",
    phoneNumber: "567890",
    type: "USER",
  },
];

//reducer là 1 hàm, state chính là userList, viết cách khác action= {type,payload} <=(state = initialState, action)
const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "ADD_USER":
      state.push(payload);
      //return về data mới khi push
      return [...state];
    case "UPDATE_USER":
      const x = state.findIndex((item) => item.id === payload.id);
      state[x] = payload;
      return [...state];
    default:
      //trả về data mới
      return state;
  }
};
export default reducer;
