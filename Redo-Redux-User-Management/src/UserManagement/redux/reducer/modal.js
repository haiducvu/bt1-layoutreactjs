//giá trị khởi tạo
let initialState = false;
const reducer = (state = initialState, action) => {
  //state là giá trị hiện tại của modal => false
  //action yêu cầu gởi lên từ component sẽ return về data mới
  const { type } = action;
  switch (type) {
    case "SHOW_DATA":
      state = true;
      return state;
    case "CANCEL":
      state = false;
      return state;
    default:
      return state;
  }
};
export default reducer;
