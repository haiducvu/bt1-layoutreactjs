//initial state
let initialState = null;
const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SELECT_USER":
      return payload;
    default:
      return state;
  }
};
export default reducer;
