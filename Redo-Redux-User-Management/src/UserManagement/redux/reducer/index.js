import { combineReducers } from "redux";
import userList from "./userList";
import showModal from "./modal";
import selectedUser from "./selectedUser";
const rootReducer = combineReducers({
  //chứa state(dữ liệu lưu trên store)
  //tenDuLieu: tenReducerQuanLyDuLieu
  userList,
  showModal,
  selectedUser,
});

export default rootReducer;
