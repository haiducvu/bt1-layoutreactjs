import axios from "axios";

const createConnector = (customConfig) => {
  const config = {};

  const token = localStorage.getItem("t");

  if (token) {
    config.headers = {
      Authorization: "Bearer " + token,
    };
  }

  console.log({ ...customConfig, ...config });

  return axios({ ...customConfig, ...config });
};

export default createConnector;
