import React, { Component } from "react";
import Home from "./pages/Home";
import Signin from "./pages/Signin";
import { connect } from "react-redux";
import { createAction } from "./redux/actions";
import { SET_TOKEN } from "./redux/actions/type";

//config router
import { BrowserRouter, Route, Switch } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/signin" component={Signin} />
          <Route path="/" component={Home} />
          <Route path="/detail" />
        </Switch>
      </BrowserRouter>
    );
  }

  componentDidMount() {
    const token = localStorage.getItem("t");
    if (token) {
      this.props.dispatch(createAction(SET_TOKEN, token));
    }
  }
}

export default connect()(App);
