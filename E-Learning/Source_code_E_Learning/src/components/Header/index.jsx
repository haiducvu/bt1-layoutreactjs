import { AppBar, Button, Toolbar } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
class Header extends Component {
  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <Button color="inherit">Home</Button>
          {this.props.isLogin ? (
            <Link to="">
              <Button color="inherit">Hello, Homie</Button>
            </Link>
          ) : (
            <>
              <Link to="/singnin">
                <Button color="inherit">Login</Button>
              </Link>
              <Button color="inherit">Signup</Button>
            </>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

const mapStateToProps = (state) => ({
  isLogin: !!state.credentials.token,
});

export default connect(mapStateToProps)(Header);
