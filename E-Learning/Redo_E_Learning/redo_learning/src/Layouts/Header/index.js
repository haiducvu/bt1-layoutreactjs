import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
export default class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
        <a className="navbar-brand" href="#">
          Hitachi
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <NavLink
                activeStyle={{ color: "red" }}
                exact
                to="/"
                className="nav-link"
              >
                Home
              </NavLink>
            </li>
          </ul>
          <ul className="navbar-nav  ">
            <li className="nav-item">
              <NavLink
                activeStyle={{ color: "red" }}
                exact
                to="/signup"
                className="nav-link"
                href="/signup"
              >
                SignUp
              </NavLink>
            </li>
            <li className="nav-item">
            <NavLink
            activeStyle={{ color: "red" }}
            exact
            to="/signin"
            className="nav-link"
            href="/signin"
          >
            Đăng Nhập
          </NavLink>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0"></form>
        </div>
      </nav>
    );
  }
}
