import React, { Component } from 'react'
import {Form, Formik, Field} from 'formik'
 class SigninScreen extends Component {
    render() {
        return (
            <Formik initialValues={{ taiKhoan:'', matKhau:''}} 
            onSubmit={((values)=>{
                console.log(values);
            })}
             render={({handleChange})=>
             ( <Form className="w-50 mx-auto">
            <h1>Sign In</h1>
            <div className="form-group">
                <label>Tài Khoản</label>
                <Field type="text" className="form-control" name="taiKhoan" onChange={handleChange}/>
            </div>
            <div className="form-group">
            <label>Mật Khẩu</label>
            <Field type="password" className="form-control" name="matKhau" onChange={handleChange}/>
            </div>
            <button className="btn btn-success">Đăng nhập</button>
        </Form>)}> 
            </Formik>
        )
    }
}
export default SigninScreen;