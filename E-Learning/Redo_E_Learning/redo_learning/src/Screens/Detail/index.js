import Axios from "axios";
import React, { Component } from "react";
import { connect } from "react-redux";
import { courseService } from "../../Service";
import { FETCH_COURSE_DETAIL } from "../../Redux/Actions/type";
import { fetchDetailCourses } from "../../Redux/Actions/user";
class CourseDetailScreen extends Component {
  render() {
    const { maKhoaHoc, tenKhoaHoc, hinhAnh } = this.props.courseDetail;
    return (
      <div>
        <img src={hinhAnh} alt="course detail" />
        <h3>{tenKhoaHoc}</h3>
      </div>
    );
  }
  componentDidMount() {
    this.props.dispatch(fetchDetailCourses(this.props.match.params.courseId));
  }
}
const mapStateToProps = (state) => ({
  //tạo ra 1 courseDetail mới để dùng
  courseDetail: state.course.courseDetail || {
    maKhoaHoc: "",
    tenKhoaHoc: "",
    hinhAnh: "",
    nguoiTao: {
      taiKhoan: "",
      hoTen: "",
    },
  },
});
export default connect(mapStateToProps)(CourseDetailScreen);
