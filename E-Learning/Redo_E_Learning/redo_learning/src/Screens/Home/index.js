import React, { Component } from "react";
import CourseItem from "./../../Components/CourseItems";
//dispatch ds lên store
import { connect } from "react-redux";
import { fetchCourses } from "../../Redux/Actions/user"; 
class Home extends Component {
  renderCourses = () => {
    return this.props.courseList.map((item, index) => {
      return (
        <div className="col-3">
          <CourseItem item={item} key={index} />
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <h1 className="display-4 text-center">Danh sách khóa học</h1>
        <div className="container">
          <div className="row">{this.renderCourses()}</div>
        </div>
      </div>
    );
  }
  //life cycle chạy sau render và chạy đúng 1 lần đầu tiên
  // lấy data về
  componentDidMount() {
    //axios return promise ES6
    this.props.dispatch(fetchCourses());
  }
}
const mapStateToProps = (state) => ({
  courseList: state.course.courses,
});
export default connect(mapStateToProps)(Home);
