import { createAction } from ".";
import { courseService } from "../../Service";
import { FETCH_COURSES, FETCH_COURSE_DETAIL } from "./type";

//acsnc action
export const fetchCourses = () => {
  return (dispatch) => {
    courseService
      .fetchCourses()
      .then((res) => {
        dispatch(createAction(FETCH_COURSES, res.data));
        //console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const fetchDetailCourses =(id)=>{
  return dispatch => {
    courseService
    .fetchCoursesDetail()
    .then((res) => {
      dispatch(FETCH_COURSE_DETAIL, res.data);
      //console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
  }
}