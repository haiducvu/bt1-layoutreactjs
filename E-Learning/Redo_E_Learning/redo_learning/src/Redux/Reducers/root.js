import { combineReducers } from "redux";
import CourseReducer from "./course";
const RootReducer = combineReducers({
  //tạo 1 biến để lưu danh sách trên store
  course: CourseReducer,
});

export default RootReducer;
