import React from "react"; //can use Fragment
import CourseDetailScreen from "./Screens/Detail";
import Home from "./Screens/Home";
import SignupScreen from "./Screens/Signup";
import Header from "./Layouts/Header";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import SigninScreen from "./Screens/Signin";
function App() {
  return (
    <>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/signup" component={SignupScreen} />
          <Route path="/signin" component={SigninScreen}/>
          <Route path="/detail/:courseId" component={CourseDetailScreen} />
          <Route path="/" component={Home} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
//test
