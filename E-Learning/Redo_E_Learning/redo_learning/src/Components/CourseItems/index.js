import React, { Component } from "react";
import {Link} from "react-router-dom";
export default class CourseItem extends Component {
  render() {
    const { tenKhoaHoc, nguoiTao, hinhAnh } = this.props.item;
    return (
      <div className="card p-2">
        <img src={hinhAnh} style={{ width: "100%", height: 200 }} alt="oiu" />
        <p className="lead font-weight-bold">{tenKhoaHoc} </p>
        <p className="lead">{nguoiTao.hoTen}</p>
        <Link to={`/detail/${this.props.item.maKhoaHoc}`} className="btn btn-success">Go to detail</Link>
      </div>
    );
  }
}
