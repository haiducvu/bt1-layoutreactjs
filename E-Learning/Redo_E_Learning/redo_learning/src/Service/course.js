import Axios from "axios";
class CourseService {
  //home screen
  fetchCourses() {
    return Axios({
      method: "GET",
      url:
        "https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01",
    });
  }
  //detail screen
  fetchCoursesDetail(id) {
    return Axios({
      method: "GET",
      //hardcode maKhoaHoc
      url:
        `https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`,
    });
  }
}
export default CourseService;
