import Axios from "axios";
import * as yup from "yup";
export const signupUserSchema = yup.object().shape({
  taiKhoan: yup.string().required("*Field is required").nullable(),
  matKhau: yup.string().required("*Field is required").nullable(),
  hoTen: yup.string().required("*Field is required").nullable(),
  email: yup
    .string()
    .required("*Field is required")
    .email("Email is invalid")
    .nullable(),
  soDT: yup
    .string()
    .required("*soDt is required")
    .matches(/^[0-9]+$/),
  //maLoaiNguoiDung: yup.string().required("*Field is required").nullable(),
  maNhom: yup.string().required("*Field is required").nullable(),
});

class UserService {
  sigUp(data) {
    return Axios({
      method: "POST",
      //url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
      url:"https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      data,
    });
  }
  signIn(user){
    return Axios({
      method: "POST",
      //url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      url:"https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      user,
    });
  }
}
export default UserService;
