import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Header from "./Header/Header";
import Silder from "./Slider/Silder";
import ProductList from "./Product/ProductList";

import Footer from "./Footer/Footer";
function App() {
  return (
    <div className="container">
      <h1 className="display-3 text-center">***</h1>
      <Header />
      <Silder />
      <ProductList />

      <Footer />
    </div>
  );
}

export default App;
