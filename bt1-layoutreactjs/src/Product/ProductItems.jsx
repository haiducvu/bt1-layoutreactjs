import React, { Component } from "react";

export default class ProductItems extends Component {
  render() {
    const {
      id,
      name,
      price,
      screen,
      backCamera,
      frontCamera,
      img,
      desc,
    } = this.props.item;
    return (
      <div className="card mb-4">
        <h5>{id}</h5>
        <h6>{name}</h6>
        <img src={img} atl="" />
        <h6>{price}</h6>
        <h6>{screen}</h6>
        <h6>{backCamera}</h6>
        <h6>{frontCamera}</h6>
        <h6>{desc}</h6>
      </div>
    );
  }
}
