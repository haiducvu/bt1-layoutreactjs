import React, { Component } from "react";

export default class Silder extends Component {
  render() {
    return (
      <div>
        <h1>Slider</h1>
        <div id="myCarousel" className="carousel slide" data-ride="carousel">
          <ol className="carousel-indicators">
            <li
              data-target="#myCarousel"
              data-slide-to="0"
              className="active"
            ></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner">
            <div className="item active">
              <img src="./img/img/slide_1.jpg" alt="cybersoft1" />
            </div>
          </div>
          {/**
                  <div className="carousel-inner">
            <div className="item active">
              <img src="./img/img/slide_2.jpg" alt="cybersoft2" />
            </div>
          </div>
          <div className="carousel-inner">
            <div className="item active">
              <img src="./img/img/slide_3.jpg" alt="cybersoft3" />
            </div>
          </div>
        */}
        </div>
        <a
          className="left carousel-control"
          href="#myCarousel"
          data-slide="prev"
        >
          <span className="glyphicon glyphicon-chevron-left"></span>
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="right carousel-control"
          href="#myCarousel"
          data-slide="next"
        >
          <span className="glyphicon glyphicon-chevron-right"></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}
