import React, { Component } from "react";
import Categories from "../../Component/Categories/Categories";
import Model from "../../Component/Model/Model";
import Product from "../../Component/Products/Product";
import Footer from "../../Layouts/Footer/Footer";
import Header from "../../Layouts/Header/Header";
export default class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container-fluid">
          <div className="row">
            <div className="col-6">
              <Categories />
              <Product />
            </div>
            <div className="col-6">
              <Model />
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
