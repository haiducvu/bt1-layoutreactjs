import React, { memo } from "react";

const DemoHookChildren = (props) => {
  console.log("DemohookChildren re-render!");
  return (
    <div>
      <h1>Demo Hook Children</h1>

      <button className="btn btn-success" onClick={props.calcSum}>
        Caculate Sum
      </button>
    </div>
  );
};

export default memo(DemoHookChildren);
