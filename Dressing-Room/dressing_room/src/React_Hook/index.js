import React, { useState, useEffect, useCallback } from "react";
import DemoHookChildren from "./child";
/**
 *Class Component: có state+ lifecycle
 *React_Hook Ra đời giúp sử dụng state và lifecycle trong Function Component
 *
 */

const DemoHook = () => {
  //   //sử dụng useState trong fc
  //   //useState trả về 1 array,giá trị ban đầu là 0
  //   const [count, setCount] = useState(0);
  //   const [isAgree, setIsAgree] = useState(false);
  //   //viết hàm  increase
  //   const increaseCount = () => {
  //     if (isAgree) {
  //       setCount(count + 1);
  //     }
  //   };

  //   const increaseCountCallback = useCallback(() => increaseCount, []);

  //   //viết hàm change count
  //   const agreeToChangeCount = () => {
  //     setIsAgree(true);
  //   };
  //   //trường hợp fectch data từ api, chỉ muốn fetch duy nhất 1 lần, ta gặp thêm [] [1 component có rất nhiều useEffect]
  //   //=> đó chính didMount
  //   useEffect(() => {
  //     console.log("useEffect run! 1 time");
  //   }, []);

  //   //đại diện cho 3 lifecycle: dismount, didupdate, willmount
  //   useEffect(() => {
  //     //code
  //     console.log("useEffect run!");
  //   });
  //   //mong muốn cho hàm change count chỉ render 1 lần, chỉ cần truyền biến isAgress vô mảng giúp render chỉ 1 lần
  //   useEffect(() => {
  //     console.log("useEffect just run when isAgree change!");
  //   }, [isAgree]);
  //   return (
  //     <div>
  //       <h1>React_Hook Demo_UseState</h1>
  //       <button className="btn btn-success" onClick={increaseCount}>
  //         Increase Count
  //       </button>
  //       <button className="btn btn-success" onClick={agreeToChangeCount}>
  //         Agree change count
  //       </button>
  //       <h2 className="display-4">Count: {count}</h2>
  //       <DemoHookChildren count={count} increaseCount={increaseCountCallback} />
  //     </div>
  //   );

  // useCallBack-P2
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const calcSum = () => {
    console.log(num1 + num2);
    return num1 + num2;
  };
  const calcSumCallback = useCallback(() => calcSum(), [num1]); //truyền vô bản sao của calcSum
  return (
    <div>
      <DemoHookChildren calcSum={calcSumCallback} />
      <button
        onClick={() => {
          setNum1(num1 + 2);
          console.log(num1);
        }}
      >
        Change Num 1
      </button>
    </div>
  );
};

export default DemoHook;
