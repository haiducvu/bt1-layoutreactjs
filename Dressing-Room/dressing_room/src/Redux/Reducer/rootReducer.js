import { combineReducers } from "redux";
import ProducrReducer from "./product";
import CategoryReducer from "./categories";
import ChooseCatagories from "./chooseCatagories";
import ModelReducer from "./model";
const RootReducer = combineReducers({
  //danh sách state lưu trữ trên store
  products: ProducrReducer,
  categories: CategoryReducer,
  chooseCatagories: ChooseCatagories,
  model: ModelReducer,
});
export default RootReducer;
