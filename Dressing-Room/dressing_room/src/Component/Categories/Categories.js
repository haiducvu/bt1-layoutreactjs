import React, { Component } from "react";
//Muốn sử dụng data trên store.. ta phải connect với reducer
import { connect } from "react-redux";
class Categories extends Component {
  _chooseCategory = (payload) => {
    this.props.dispatch({
      type: "SET_CATEGORY",
      payload,
    });
  };
  render() {
    return (
      <div className="btn-group">
        {this.props.categoriesList.map((item, index) => (
          <button
            className={
              this.props.chooseCatagories === item.type
                ? "btn btn-danger"
                : "btn btn-warning"
            }
            key={index}
            onClick={() => {
              this._chooseCategory(item.type);
            }}
          >
            {item.showName}
          </button>
        ))}

        {/**
         <button className="btn btn-secondary">Áo</button>
        <button className="btn btn-secondary">Tóc</button>
      */}
      </div>
    );
  }
}
//hiển thị trên store , ta có hàm lấy data trên store về
const mapStateToProps = (state) => ({
  //trả về 1 object
  categoriesList: state.categories,

  chooseCatagories: state.chooseCatagories,
});
export default connect(mapStateToProps)(Categories);
