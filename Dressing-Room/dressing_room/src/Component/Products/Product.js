import React, { Component } from "react";
import ProductItem from "../ProductItem/ProductItem";
//connect component với store
import { connect } from "react-redux";
class Product extends Component {
  render() {
    return (
      <div>
        <div className="row">
          {/**Render data lấy từ store về */}
          {this.props.productList
            .filter((item) => item.type === this.props.chooseCatagories)
            .map((item, index) => (
              <div className="col-3" key={index}>
                <ProductItem item={item} />
              </div>
            ))}
          {/**
          <div className="col-3">
            <ProductItem />
          </div>
          <div className="col-3">
            <ProductItem />
          </div>
          <div className="col-3">
            <ProductItem />
          </div>
          <div className="col-3">
            <ProductItem />
          </div>
         */}
        </div>
      </div>
    );
  }
}

//lấy data từ store lên sài
const mapSateToProps = (state) => ({
  //nhận vào state và reutrn về 1 object
  productList: state.products,
  //bây giờ ta đã có 1 props mới productList lấy từ trên store về

  // khi ta muốn lụm chooseCategory xuống để biết mình chọn cái gì
  chooseCatagories: state.chooseCatagories,
});
export default connect(mapSateToProps)(Product);
