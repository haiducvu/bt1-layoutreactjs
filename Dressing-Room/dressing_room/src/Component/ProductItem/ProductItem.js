import React, { Component } from "react";
//connect với store
import { connect } from "react-redux";
class ProductItem extends Component {
  _chooseCloth = (cloth) => {
    this.props.dispatch({
      type: "SET_CLOTH",
      payload: {
        type: cloth.type,
        img: cloth.imgSrc_png,
      },
    });
  };
  render() {
    const { id, type, name, desc, imgSrc_jpg, imgSrc_png } = this.props.item;
    return (
      <div className="card p-2 my-2">
        <img src={imgSrc_jpg} alt={"dressingroom"} />
        <p>{name}</p>
        <button
          className="btn btn-success"
          onClick={() => this._chooseCloth(this.props.item)}
        >
          Thử đồ
        </button>
      </div>
    );
  }
}

export default connect()(ProductItem);
