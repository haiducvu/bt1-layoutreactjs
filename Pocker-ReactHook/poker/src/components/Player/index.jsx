import React from "react";
import Card from "../Card";

const Player = (props) => {
  return (
    <div className={`player-${props.index}`}>
      <p className="lead">Player Name</p>
      <main className="d-flex">
        <Card />
        <Card />
        <Card />
      </main>
    </div>
  );
};

export default Player;
