import React, { useState } from "react";
import Game from "../Game";
//connect với store, với hook dùng connect
import { useDispatch } from "react-redux";
const Home = () => {
  //sate thứ 1
  const [gameStarted, setGameStarted] = useState(false);
  //tạo thêm sate thứ 2
  const [name, setName] = useState("");
  const dispatch = useDispatch();
  // lấy setGame và truyền giá trị mới vào
  const handleStartGame = () => {
    setGameStarted(true);
    dispatch({
      type: "SET_NAME",
      payload: name,
    });
  };
  //lấy name và set lại
  const handleChange = (e) => {
    setName(e.target.value);
  };
  return (
    <>
      {gameStarted ? (
        <Game />
      ) : (
        <div
          className="text-center"
          style={{
            width: "100vw",
            height: "100vh",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <h1 className="diplay-4 mb-5"> Welcome to Pocker Center</h1>
          <h3>Fill your name and start</h3>
          <input
            onChange={handleChange}
            type="input"
            className="w-25 form-control mb-3"
          />
          <button className="btn btn-success" onClick={handleStartGame}>
            Start new Game
          </button>
        </div>
      )}
    </>
  );
};

export default Home;
